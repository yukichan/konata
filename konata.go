package main

import (
	"gitlab.com/yukichan/konata/config"
	"gitlab.com/yukichan/konata/mumbleconnection"
)

func main() {
	keepAlive := make(chan bool)

	config := config.LoadConfig("/etc/konata.toml")

	client := mumbleconnection.NewClient(&config)
	client.Connect()

	<-keepAlive
}
