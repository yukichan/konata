package mumbleconnection

import (
	"github.com/layeh/gumble/gumble"
	"gitlab.com/yukichan/konata/config"
	"gitlab.com/yukichan/konata/interpreter"
	"gitlab.com/yukichan/konata/mediaman"
	"log"
)

func listenerFunc(e interface{}, conf *config.Config) {
	switch e.(type) {
	case *gumble.UserChangeEvent:
		handleUserChange(e.(*gumble.UserChangeEvent), conf)
	case *gumble.TextMessageEvent:
		handleMessage(e.(*gumble.TextMessageEvent), conf)
	default:
		//log.Printf("%T\n", e)
	}
}

func handleMessage(e *gumble.TextMessageEvent, conf *config.Config) {
	message := e.TextMessage
	user := message.Sender
	if user == nil {
		log.Printf("Recieved message from null user, ignoring...")
		return
	}
	interpreter.InterpretMessage(message.Message, user, e.Client, conf)
}

func handleUserChange(e *gumble.UserChangeEvent, conf *config.Config) {
	if e.Type&gumble.UserChangeConnected == 1 {
		//A user has connected!
		connectedUser := e.User
		log.Printf("User %s has connected", connectedUser.Name)
		mediaman.PlaySound("connect", connectedUser.Name, conf, e.Client)
	}
}

func initListener(conf *config.Config, c *gumble.Client) {
	go mediaman.PlaySoundQueue(conf, c)
}
