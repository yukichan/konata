package mumbleconnection

import (
	"crypto/tls"
	"fmt"
	"github.com/layeh/gumble/gumble"
	"github.com/layeh/gumble/gumbleutil"
	_ "github.com/layeh/gumble/opus"
	"gitlab.com/yukichan/konata/config"
	"log"
	"net"
)

type Client struct {
	gumbleConf   *gumble.Config
	gumbleClient *gumble.Client
	konataConf   *config.Config
	dialer       *net.Dialer
	tlsConf      *tls.Config
	keyPair      *tls.Certificate
}

func NewClient(configuration *config.Config) *Client {
	var res Client
	var dialer net.Dialer
	var conftls tls.Config

	certpath := fmt.Sprintf("%s/cert.pem", configuration.CertPath)
	keypath := fmt.Sprintf("%s/key.pem", configuration.CertPath)

	res.gumbleConf = gumble.NewConfig()
	res.gumbleConf.Username = configuration.ClientName

	res.konataConf = configuration
	res.dialer = &dialer
	res.tlsConf = &conftls

	keyPair, err := tls.LoadX509KeyPair(certpath, keypath)
	if err != nil {
		log.Printf("Could not load key and/or certificate from %s; continuing without a certificate...", configuration.CertPath)
	} else {
		res.keyPair = &keyPair
	}

	return &res
}

func (c *Client) Connect() {
	serverAddr := fmt.Sprintf("%s:%d", c.konataConf.ServerAddr, c.konataConf.ServerPort)

	c.gumbleConf.Attach(gumbleutil.ListenerFunc(
		func(e interface{}) {
			listenerFunc(e, c.konataConf)
		}))
	c.tlsConf.InsecureSkipVerify = true
	if c.keyPair != nil {
		c.tlsConf.Certificates = []tls.Certificate{*c.keyPair}
	}

	client, err := gumble.DialWithDialer(c.dialer, serverAddr, c.gumbleConf, c.tlsConf)
	if err != nil {
		log.Fatalf("Could not connect to server: failed with error %s", err)
	} else {
		c.gumbleClient = client
	}
	initListener(c.konataConf, c.gumbleClient)
}
