package mediaman

import (
	"errors"
	"fmt"
	"github.com/layeh/gumble/gumble"
	"github.com/layeh/gumble/gumbleffmpeg"
	"gitlab.com/yukichan/konata/config"
	"log"
	"os"
)

type soundEvent struct {
	event    string
	username string
}

var soundQueues = []chan soundEvent{
	make(chan soundEvent),
	make(chan soundEvent),
}

/*
** Decide which audio file to play given an event name and username
 */
func FindFile(event, username string, conf *config.Config) (string, error) {
	dirname := fmt.Sprintf("%s/%s", conf.SoundRoot, event)
	if _, err := os.Stat(dirname); os.IsNotExist(err) {
		return "", errors.New(fmt.Sprintf("Could not find the directory at %s", dirname))
	}

	filename := fmt.Sprintf("%s/%s.mp3", dirname, username)
	if _, err := os.Stat(filename); !os.IsNotExist(err) {
		return filename, nil
	}

	filenamed := fmt.Sprintf("%s/default.mp3", dirname)
	if _, err := os.Stat(filenamed); !os.IsNotExist(err) {
		return filenamed, nil
	}

	return filename, errors.New("That event does not have a user specific sound or default.")
}

/*
** Create a SoundEvent object and add it to the queue of events to be played
 */
func PlaySound(event, username string, conf *config.Config, client *gumble.Client) {
	priority := len(soundQueues) - conf.Priorities[event] - 1
	if priority < 0 {
		priority = 0
	}

	newEvent := soundEvent{event: event, username: username}
	soundQueues[priority] <- newEvent
}

/*
** Play a sound file to the gumble client given a soundEvent object
 */
func PlaySoundQueueItem(e soundEvent, conf *config.Config, client *gumble.Client) {
	event := e.event
	username := e.username

	//Locate the file to play
	file, err := FindFile(event, username, conf)
	if err != nil {
		log.Printf("Could not locate file for event: %s", err)
	}

	//Try to play the file
	soundSrc := gumbleffmpeg.SourceFile(file)
	soundStr := gumbleffmpeg.New(client, soundSrc)
	err = soundStr.Play()
	if err != nil {
		log.Printf("Could not play file %s for event: %s", file, err)
	}

	//Wait for playback to finish then stop
	log.Printf("Now playing %s for event %s:%s", file, username, event)
	soundStr.Wait()
	soundStr.Stop()
}

/*
** Constantly check the queues for more sounds to play
 */
func PlaySoundQueue(conf *config.Config, client *gumble.Client) {
	for {
		var nextEvent soundEvent
		select {
		case nextEvent = <-soundQueues[0]:
			PlaySoundQueueItem(nextEvent, conf, client)
		case nextEvent = <-soundQueues[1]:
			PlaySoundQueueItem(nextEvent, conf, client)
		}
	}
}
