package interpreter

import (
	"crypto/md5"
	"fmt"
	"github.com/layeh/gumble/gumble"
	"gitlab.com/yukichan/konata/config"
	"gitlab.com/yukichan/konata/mediaman"
	"io"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var YT_DL_BIN = "youtube-dl"
var FF_MPG_BIN = "ffmpeg"

func downloadVideo(class string, name string, command []string, client *gumble.Client, conf *config.Config) string {
	//Command format: !setyoutube <type> <starttime> <endtime> <url>
	event := command[1]
	starttime := command[2]
	endtime := command[3]
	url := strings.Join(command[4:len(command)], " ")
	url = rmHyperlink(url)

	targetFile, err := mediaman.FindFile(event, name, conf)
	if err != nil && string(err.Error()) != "That event does not have a user specific sound or default." {
		log.Printf("Could not find file to use")
		return "Stop trying to break Rem!"
	}

	startsecs := timeToSecs(starttime)
	endsecs := timeToSecs(endtime)
	if conf.MaxClipTime > 0 && (endsecs-startsecs > conf.MaxClipTime) {
		return fmt.Sprintf("Please make sure you limit the size of your clips to %d seconds or less. Nice try wanker.", conf.MaxClipTime)
	}

	tmpfile := runDownload(url, startsecs, endsecs, conf)
	copyToDest(tmpfile, targetFile)
	return ("User sound updated.")
}

func copyToDest(tmpfile string, tgtfile string) {
	srcFile, err := os.Open(tmpfile)
	defer srcFile.Close()
	if err != nil {
		log.Printf("Could not load temporary file: %s", err)
		return
	}

	destFile, err := os.Create(tgtfile) // creates if file doesn't exist
	defer destFile.Close()
	if err != nil {
		log.Printf("Could create target file: %s", err)
		return
	}

	_, err = io.Copy(destFile, srcFile) // check first var for number of bytes copied
	if err != nil {
		log.Printf("Could copy to target file: %s", err)
		return
	}

	err = destFile.Sync()
	if err != nil {
		log.Printf("Sync error: %s", err)
		return
	}

	srcFile.Close()
	os.Remove(tmpfile)

}

func timeToSecs(timestamp string) int {
	times := strings.Split(timestamp, ":")
	var res int = 0
	multiplier := 1

	if len(times) <= 0 {
		res, _ := strconv.Atoi(timestamp)
		return res
	}

	for i := len(times) - 1; i >= 0; i-- {
		cnt, _ := strconv.Atoi(times[i])
		res += cnt * multiplier
		multiplier *= 60
	}
	return res
}

func runDownload(url string, startsecs int, endsecs int, conf *config.Config) string {
	intfile := genFileName(url, "int", conf)
	tgtfile := genFileName(url, "tgt", conf)

	log.Printf("Now downloading video from %s to temp file %s", url, intfile)

	//Download video and convert to audio
	ytcom := exec.Command(YT_DL_BIN,
		"-f bestaudio",
		"-x",
		"--audio-format",
		"mp3",
		url,
		"-o",
		fmt.Sprintf("%s.%%(ext)s", intfile))

	out, err := ytcom.CombinedOutput()
	if err != nil {
		log.Printf("Failed to fetch video: %s, youtube-dl outputted error %s", out, err)
	}

	//Crop audio clip
	duration := endsecs - startsecs
	if duration <= 0 {
		duration = 1
	}
	log.Printf("Now cropping clip from time %d to %d", startsecs, endsecs)
	cropcom := exec.Command(FF_MPG_BIN,
		"-y",
		"-ss",
		fmt.Sprintf("%d", startsecs),
		"-t",
		fmt.Sprintf("%d", duration),
		"-i",
		fmt.Sprintf("%s.mp3", intfile),
		fmt.Sprintf("%s.mp3", tgtfile))

	out, err = cropcom.CombinedOutput()
	if err != nil {
		log.Printf("Failed to crop video: %s, ffmpeg outputted error %s", out, err)
	}

	//Delete intermediate file
	os.Remove(intfile)
	return fmt.Sprintf("%s.mp3", tgtfile)
}

func genFileName(url string, suffix string, conf *config.Config) string {
	data := []byte(url)
	randData := md5.Sum(data)
	randExt := fmt.Sprintf("%x", randData)
	randExt = randExt[:10]
	filename := fmt.Sprintf("%s%s%s", conf.TmpRoot, suffix, randExt)
	return filename
}
