package interpreter

import (
	"github.com/layeh/gumble/gumble"
	"gitlab.com/yukichan/konata/config"
	"log"
	"strings"
)

func InterpretMessage(m string, user *gumble.User, client *gumble.Client, conf *config.Config) {
	name := user.Name
	class := conf.UserClass[name]
	log.Printf("Got message '%s' from user '%s' who is of class '%s'", m, name, class)
	comS := conf.CommandSpecifier

	//Easter egg!
	if m == "I love you Rem" {
		channel := client.Self.Channel
		channel.Send("But I love Emilia </3", false)
	}

	if m[0:len(comS)] != comS {
		return
	}

	command := m[1 : len(m)-1]
	com := strings.Split(command, " ")
	log.Printf("Now executing '%s' as %s", command, name)
	switch com[0] {
	case "setsound":
		setSound(class, name, com, client, conf)
		user.Send("Intro successfully updated!")
	case "setyoutube":
		msg := downloadVideo(class, name, com, client, conf)
		user.Send(msg)
	}
}
