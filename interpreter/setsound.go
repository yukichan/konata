package interpreter

import (
	"github.com/layeh/gumble/gumble"
	"gitlab.com/yukichan/konata/config"
	"gitlab.com/yukichan/konata/mediaman"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

func setSound(class string, name string, command []string, client *gumble.Client, conf *config.Config) {
	//Command format: !setsound <type> <fetchloc> <url>
	event := command[1]
	fetchloc := command[2]
	url := strings.Join(command[3:len(command)], " ")

	targetFile, err := mediaman.FindFile(event, name, conf)
	if err != nil && string(err.Error()) != "That event does not have a user specific sound or default." {
		log.Printf("Could not find file to use")
		return
	}

	switch fetchloc {
	case "mp3":
		dlMp3(targetFile, url, conf.FilePerms)
	}
}

func dlMp3(tgt string, url string, permissions int) {
	//Delete old file
	os.Remove(tgt)
	//Open file
	tgtFile, err := os.OpenFile(tgt, os.O_CREATE|os.O_WRONLY, os.FileMode(permissions))
	if err != nil {
		log.Printf("Could not open file %s for reason: %s", tgt, err)
		return
	}
	defer tgtFile.Close()

	//Download the data
	url = rmHyperlink(url)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Could not get data from %s for reason: %s", url, err)
		return
	}
	defer resp.Body.Close()

	//Write to file
	log.Printf("Writing data downloaded from %s to file %s", url, tgtFile)
	_, err = io.Copy(tgtFile, resp.Body)
	if err != nil {
		log.Printf("Could not write data to %s for reason: %s", tgt, err)
		return
	}
}

func rmHyperlink(url string) string {
	if url[0] == '<' {
		//Mumble has hyperlinked it dammit!
		url = strings.TrimPrefix(url, "<a href=")
		url = strings.Split(url, ">")[0]
		url = strings.Trim(url, "\"")
		return url
	} else {
		return url
	}
}
