package config

import (
	"github.com/BurntSushi/toml"
	"io/ioutil"
	"log"
)

type Config struct {
	ServerAddr       string            `toml:"ServerAddress"`
	ServerPort       int               `toml:"ServerPort"`
	ClientName       string            `toml:"BotName"`
	SoundRoot        string            `toml:"SoundFileRoot"`
	CertPath         string            `toml:"CertPath"`
	Priorities       map[string]int    `toml:"EventPriority"`
	UserClass        map[string]string `toml:"UserClass"`
	CommandSpecifier string            `toml:"CommandSpecifier"`
	FilePerms        int               `toml:"SoundFilePermissions"`
	TmpRoot          string            `toml:"TmpRoot"`
	MaxClipTime      int               `toml:"MaxClipTime"`
}

func LoadConfig(loc string) Config {
	file, err := ioutil.ReadFile(loc)
	if err != nil {
		log.Fatalf("Could not load config file at %s", loc)
	}
	var res Config
	if _, err := toml.Decode(string(file), &res); err != nil {
		log.Fatalf("Could not parse config file at %s", loc)
	}

	return res
}
